package collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class Test {
	public static void main(String []args){
		String values = "23,4,6,12,5,4,16,12,6";
		String[] arr = values.split(",");
		
		Collection<String> col = new ArrayList();
		
		for(String str: arr){
			col.add(str);
		}
		
		System.out.println(col);
		
		col.add("4");
		
		System.out.println(col);
		
		//Test Set
		Set<String> set = new HashSet();
		
		for(String str: arr){
			set.add(str);
		}
		System.out.println("Set: "+set);
		
		set.add("4");
		System.out.println("Set: "+set);
		
		//Test LinkedSet
		Set<String> linkedSet = new LinkedHashSet<>();
		
		for(String str: arr){
			linkedSet.add(str);
		}
		System.out.println("linkedSet: "+linkedSet);
		
		linkedSet.add("15");
		System.out.println("linkedSet: "+linkedSet);
		
		//Test TreeSet 
				Set<String> treeSet = new TreeSet<>();
				
				for(String str: arr){
					treeSet.add(str);
				}
				System.out.println("treeSet: "+treeSet);
				
				treeSet.add("15");
				System.out.println("treeSet: "+treeSet);
		
				
				//Test TreeSet with comparator
				Set<String> treeSet2 = new TreeSet<>(new NumberComparator());
				
				for(String str: arr){
					treeSet2.add(str);
				}
				System.out.println("treeSet Comp: "+treeSet2);
				
				treeSet2.add("15");
				System.out.println("treeSet Comp: "+treeSet2);
		
				//queue List
			    Queue<String> queue = new LinkedList<>();
			    
				for(String str: arr){
					queue.add(str);
				}
				System.out.println("Queue: "+queue);
				
				System.out.println("Removed: "+queue.remove());
				
				System.out.println("Queue: "+queue);
		
				//pqueue List
			    Queue<String> pqueue = new PriorityQueue<>();
			    
				for(String str: arr){
					pqueue.add(str);
				}
				System.out.println("Priority Queue: "+pqueue);
				
				System.out.println("Removed: "+pqueue.remove());
				
				System.out.println("Priority Queue: "+pqueue);
				
				//pqueue with number comparator
			    Queue<String> pcqueue = new PriorityQueue<>(new NumberComparator());
			    
				for(String str: arr){
					pcqueue.add(str);
				}
				System.out.println("Priority Queue Comp: "+pcqueue);
				
				System.out.println("Removed: "+pcqueue.remove());
				
				System.out.println("Priority Queue Comp: "+pcqueue);
				//stack
				Queue stack = Collections.asLifoQueue(new LinkedList());
				 
				for(String str: arr){
					stack.add(str);
				}
				System.out.println("Priority Queue Comp: "+stack);
				
				System.out.println("Removed: "+stack.remove());
				
				System.out.println("Priority Queue Comp: "+stack);
	}

}
